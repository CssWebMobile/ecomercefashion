<?php
$conexao = mysqli_connect("localhost", "root", "", "WD43");

$dados = mysqli_query($conexao, "Select * from produtos where id = $_POST[id]");
$produto = mysqli_fetch_array($dados);
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Checkout Mirror Fashion</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
       
        <link href="css/bootstrap-flatly.css" rel="stylesheet" type="text/css"/>
        <link href="css/checkout.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span> <span class="some">Mirror Fashion</span></a>
                <button class="navbar-toggle" type="button"
                        data-target=".navbar-collapse"
                        data-toggle="collapse"><spam class="glyphicon glyphicon-align-justify"  ></spam></button>
            </div>
            <ul class="nav navbar-nav  collapse navbar-collapse">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> <span class="some">Sua Conta</span></a></li>
                <li><a href="#"><span class="glyphicon glyphicon-list-alt"></span> <span class="some">Lista de Desejos</span></a></li>
                <li><a href="#"><span class="glyphicon glyphicon-credit-card"></span><span class="some"> Cartão Fidelidade</span></a></li>
                <li><a href="sobre.php"><span class="glyphicon glyphicon-cloud"></span><span class="some"> Sobre</span></a></li>
                <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span> <span class="some">Ajuda</span></a></li>
            </ul>
        </nav>

        <div class="jumbotron">
            <div class="container">
                <h1>Ótima Escolha</h1>
                <p>Obrigado por comprar na Mirror Fashion!
                    Preencha seus dados para efetivar a compra.</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-lg-3">
                    <div class="panel panel-success"> 
                        <div class="panel-heading">
                            <h2 class="panel-title">Sua Compra</h2>
                        </div>
                        <div class="panel-body">
                            <img src="img/produtos/foto<?= $_POST['id']; ?>-<?= $_POST['cor']; ?>.png"
                                 class="img-thumbnail img-responsive hidden-xs">
                            <dl>
                                <dt>Tamanho</dt>
                                <dd><?= $produto["nome"]; ?></dd>

                                <dt>Cor</dt>
                                <dd><?= $_POST["cor"]; ?></dd>

                                <dt>Tamanho</dt>
                                <dd><?= $_POST["tamanho"]; ?></dd>

                                <dt>Preco</dt>
                                <dd><?= $produto["preco"]; ?></dd>

                                <dt>ID</dt>
                                <dd><?= $produto["id"]; ?></dd>

                            </dl>
                        </div>
                    </div>
                </div>

                <form action="efetivar.php" method="POST" class="col-sm-8 col-lg-9">
                    <div class="row">
                        <fieldset class="col-md-6">
                            <legend>Dados Pessoais</legend>
                            <div class="form-group">
                                <label for="nome">Nome Completo</label>
                                <input type="text" name="nome" id="nome" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="email@exemplo.com">
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" name="cpf" id="cpf" class="form-control" placeholder="000.000.000-00" required data-mask="999.999.999-99">
                            </div>
                            <div class="checkbox">
                                <label></label>
                                <input type="checkbox"  value="sim" name="spam" checked>
                                Quero receber spam da Mirror Fashion
                            </div>
                        </fieldset>
                    </div>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <legend>Cartao de Credito</legend>

                            <div class="form-group">
                                <label for="numero-cartao">Numero - CVV</label>
                                <input type="text" name="numero-cartao" id="numero-cartao" class="form-control" data-mask="9999 9999 9999 - 999">
                            </div>


                            <div class="form-group"> 
                                <label for="bandeira-cartao">Bandeira</label>
                                <select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
                                    <option value="master">MasterCard</option>
                                    <option value="visa">Visa</option>
                                    <option value="amex">American Express</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="validade-cartao">Validade</label>
                                <input type="month" name="validade-cartao" 
                                       id="validade-cartao" class="form-control">
                            </div>

                        </fieldset>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right">
                        <spam class="glyphicon glyphicon-thumbs-up"></spam>
                        Confirmar Pedido</button>
                </form>
            </div>
        </div>
        <script src="js/checkout.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/inputmask-plugin.js" type="text/javascript"></script>
    </body>
</html>
