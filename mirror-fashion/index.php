
<?php
$cabecalho_title = "Loja Mirror Fashion";

include("cabecalho.php");
?>

<div class="container destaque">
    <section class="busca">
        <h2>Busca</h2>
        <form action="http://google.com/search" id="form-busca">
            <input type="search" name="q" id="q">
            <input type="image" src="img/busca.png">
        </form>
    </section>

    <section class="menu-departamentos">
        <h2>Departamentos</h2>
        <nav>
            <ul>
                <li><a href="#">Blusas e Camisas</a>
                    <ul>
                        <li><a href="#">CM Curtas</a></li>
                        <li><a href="#">CM Compridas</a></li>
                    </ul>

                </li>
                <li><a href="#">Calças</a></li>
                <li><a href="#">Saias</a></li>
                <li><a href="#">Vestidos</a></li>
                <li><a href="#">Sapatos</a></li>
                <li><a href="#">Bolsas e Carteiras</a></li>
                <li><a href="#">Acessorios</a></li>
            </ul>
        </nav>
    </section>

    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
                <li><img src="img/destaque-home.png" alt="fig 1"/> </li>
                <li><img src="img/destaque-home-2.png" alt="fig 1"/> </li>
            </ul>

        </div>
    </div>


</div>

<div class="container paineis">
    <section class="painel novidades">
        <h2>Novidades</h2>
        <ol>
            <?php
            $conexao = mysqli_connect("localhost", "root", "", "WD43");
            $dados = mysqli_query($conexao, "Select * from produtos ORDER BY data LIMIT 0, 12");

            while ($produto = mysqli_fetch_array($dados)):
                ?>
                <li>
                    <a href="produto.php?id=<?= $produto[id] ?>">
                        <figure>
                            <img src="img/produtos/miniatura<?= $produto[id] ?>.png" alt="<?= $produto['nome'] ?>"/>
                            <figcaption><?= $produto['nome'] . 'R$ ' . $produto['preco'] ?></figcaption>
                        </figure>
                    </a>
                </li>
            <?php endwhile; ?>
        </ol>
        <button type="button">Veja Mais</button>
    </section>

    <section class="painel mais-vendidos">
        <h2>Mais Vendidos</h2>
        <ol>
            <?php
            $conexao = mysqli_connect("localhost", "root", "", "WD43");
            $dados = mysqli_query($conexao, "Select * from produtos ORDER BY vendas DESC LIMIT 0, 12");

            while ($produto = mysqli_fetch_array($dados)):
                ?>

                <li>
                    <a href="produto.php?id=<?= $produto[id] ?>">
                        <figure>
                            <img src="img/produtos/miniatura<?= $produto[id] ?>.png" alt="<?= $produto['nome'] ?>"/>
                            <figcaption><?= $produto['nome'] . 'R$ ' . $produto['preco'] ?></figcaption>

                        </figure>
                    </a>
                </li>
            <?php endwhile; ?>
        </ol>
        <button type="button">Veja Mais</button>
    </section>
</div>
<?php include("rodape.php"); ?>
<script src="js/validacao.js" type="text/javascript"></script>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/jquery.jcarousel.min.js" type="text/javascript"></script>
<script src="js/jcarousel.basic.js" type="text/javascript"></script>
<script src="js/home.js" type="text/javascript"></script>
</body>

</html>
