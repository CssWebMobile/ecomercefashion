<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $cabecalho_title ?> </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <link href="css/mobile.css" rel="stylesheet" type="text/css" media="(max-width:939px)"/>
        <link href="css/jcarousel.basic.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=PT+Sans|Bad+Script" rel="stylesheet">
       
        <?php echo @$cabecalho_css ?>
        

    </head>
    <body>
        <header class="container">
            <h1 class="alterarFonteH1"><img src="img/logo.png" alt="logo da Mirror Fashion"/></h1>
            <div class="sacola">
                Nenhum item na sacola de compras
            </div>
            <nav class="menu-opcoes">
                <ul>
                    <li><a href="#">Sua Conta</a></li>
                    <li><a href="#">Lista de Desejos</a></li>
                    <li><a href="#">Cartão Fidelidade</a></li>
                    <li><a href="sobre.php">Sobre</a></li>
                    <li><a href="#">Ajuda</a></li>
                </ul>
            </nav>
        </header>
