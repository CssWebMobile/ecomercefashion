    <footer>
            <div class="container">
                <h1><img src="img/logo-rodape.png" alt="Logo Mirror Fashion"/></h1>
                &copy; Copyright Mirror Fashion
                <ul class="social">
                    <li><a href="http://facebook.com/mirrorfashion">Facebook</a></li>
                    <li><a href="http://twitter.com/mirrorfashion">Twitter</a></li>
                    <li><a href="http://plus.google/mirrorfashion">Google +</a></li>
                </ul>
            </div>
        </footer>