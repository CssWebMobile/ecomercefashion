<?php
$conexao = mysqli_connect("localhost", "root", "", "WD43");

$dados = mysqli_query($conexao, "Select * from produtos where id = $_GET[id]");
$produto = mysqli_fetch_array($dados);


$cabecalho_title = "$produto[nome] da Mirror Fashion";
$cabecalho_css = '<link href="css/produto.css" rel="stylesheet" type="text/css"/>';
include("cabecalho.php");
?>

<div class="produto-back">
    <div class="container">
        <div class="produto">
            <h1><?= $produto['nome']; ?></h1>
            <p>por apenas R$<?= $produto['preco']; ?></p>


            <form action="checkout.php" method="POST">
                <fieldset class="cores"><legend>Escolha a cor</legend>
                    <input type="radio" name="cor" id="verde" value="verde" checked>
                    <label for="verde"><img src="img/produtos/foto<?= $produto['id']; ?>-verde.png" alt="Casaco Verde"  /></label>

                    <input type="radio" name="cor" id="rosa" value="rosa">
                    <label for="rosa"><img src="img/produtos/foto<?= $produto['id']; ?>-rosa.png" alt="Casaco Rosa"/></label>


                    <input type="radio" name="cor" id="azul" value="azul">
                    <label for="azul"><img src="img/produtos/foto<?= $produto['id']; ?>-azul.png" alt="Casaco Azul"/></label>

                </fieldset>

                <fieldset class="tamanhos"><legend>Escolha a Tamanho:</legend>
                    <output for="tamanho" name="valortamanho">42</output>
                    <input type="range" min="36" max="46" " step="2" name="tamanho" id="tamanho">
                </fieldset>

              
                <input type="hidden" name="id" value="<?= $produto['id']; ?>">

                <input type="submit" class="comprar" value="Comprar">
            </form>

        </div>
        <div class="detalhes">
            <h2>Detalhes do Produto</h2>
            <p><?= $produto['descricao']; ?>    </p>

            <table>
                <thead>
                    <tr>
                        <th>Característica</th>
                        <th>Detalhe</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Modelo</td>
                        <td>Cardiga 78485</td>
                    </tr>
                    <tr>
                        <td>Material</td>
                        <td>Algodao e Poliester</td>
                    </tr>
                    <tr>
                        <td>Cores</td>
                        <td>Azul, Rosa e Verde</td>
                    </tr>
                    <tr>
                        <td>Lavagem</td>
                        <td>Lavar a mao</td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>
</div>
<?php include("rodape.php"); ?>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/produto.js" type="text/javascript"></script>
</body>

</html>
