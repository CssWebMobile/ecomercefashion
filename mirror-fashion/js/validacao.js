
function alteraFundo() {
    document.querySelector("#q").style.backgroundColor = "white";
}

function validaBusca() {
    var campo = document.querySelector("#q");
    if (campo.value == '') {
        campo.style.backgroundColor = "red";

        alert("Preencha o campo antes de enviar");
        return false;
    }
}

document.querySelector("#form-busca").onsubmit = validaBusca;
document.querySelector("#q").onfocus = alteraFundo;